const {SERVER_URL} = require('./serverConfig');

module.exports = {
  productionSourceMap: false,
  configureWebpack: {
    devtool: 'source-map',
  },
  devServer: {
    proxy: {
      '/movies': {
        target: SERVER_URL,
        changeOrigin: true,
      },
      '/files': {
        target: SERVER_URL,
        changeOrigin: true,
      },
      '/download': {
        target: SERVER_URL,
        changeOrigin: true,
      },
      '/uploadFiles': {
        target: SERVER_URL,
        changeOrigin: true,
      },
    },
  },
};
