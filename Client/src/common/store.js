import Vue from 'vue';
import Vuex from 'vuex';
import api from '../../api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUser: null,
    favorites: [],
  },
  mutations: {
    SET_CURRENT_USER(state, currentUser) {
      state.currentUser = currentUser;
    },
    ADD_MOVIE_TO_FAVORITES(state, movie) {
      state.favorites.push(movie);
    },
    REMOVE_MOVIE_FROM_FAVORITES(state, movie) {
      state.favorites.splice(state.favorites.indexOf(movie), 1);
    },
    INITIALIZE_FAVORITE_MOVIES(state, favorites) {
      state.favorites = [];
      favorites.forEach((favorite) => state.favorites.push(favorite));
    },
  },
  actions: {
    updateCurrentUser({commit}, currentUser) {
      commit('SET_CURRENT_USER', currentUser);
    },
    resetCurrentUser({commit}) {
      commit('SET_CURRENT_USER', null);
    },
    addMovieToFavorites({commit}, movie) {
      api.users().addToFavorites(movie._id);
      commit('ADD_MOVIE_TO_FAVORITES', movie);
    },
    removeMovieFromFavorites({commit}, movie) {
      api.users().removeFromFavorites(movie._id);
      commit('REMOVE_MOVIE_FROM_FAVORITES', movie);
    },
    loadFavorites({commit}) {
      api
          .movies()
          .getFavorites()
          .then(({data: movies}) => {
            commit(
                'INITIALIZE_FAVORITE_MOVIES',
                movies,
            );
          });
    },
  },
});
