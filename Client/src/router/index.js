import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home';
import Login from '../views/Login';
import Watch from '../views/Watch';
import Favorites from '../views/Favorites';
import Upload from '../views/Upload';
import SignUp from '../views/SignUp';
import AdminPage from '../views/AdminPage';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Login,
    name: 'login',
  },
  {
    path: '/signUp',
    component: SignUp,
    name: 'signup',
  },
  {
    path: '/home',
    component: Home,
    name: 'home',
  },
  {
    path: '/watch',
    component: Watch,
    name: 'watch',
  },
  {
    path: '/favorites',
    component: Favorites,
    name: 'favorites',
  },
  {
    path: '/upload',
    component: Upload,
    name: 'upload',
  },
  {
    path: '/adminPage',
    component: AdminPage,
    name: 'adminPage',
  },
];

const router = new VueRouter({
  linkActiveClass: 'active',
  mode: 'history',
  routes,
});

export default router;
