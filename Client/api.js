import axios from 'axios';

axios.defaults.baseURL = 'https://calm-refuge-23192.herokuapp.com/';
const updateAuthorizationHeader = () => {
  axios.defaults.headers.common[
      'Authorization'
  ] = `Bearer ${localStorage.getItem('access_token')}`;
};

export default {
  updateAuthorizationHeader,
  movies() {
    return {
      getMoviesAtPage: (pageNumber) => axios.get(`/movies/pages/${pageNumber}`),
      getMovie: (movieId) => axios.get(`/movies/movie/${movieId}`),
      getPoster: (movieId) => axios.get(`/movies/poster/${movieId}`),
      getFavorites: () => axios.get('/movies/favorites'),
      uploadMovie: (formData) => axios.post('/movies/upload', formData),
      deleteMovie: (id) => axios.delete(`/movies/delete/${id}`),
    };
  },
  users() {
    return {
      login: (credentials) => axios.post('/users/login', credentials),
      signUp: (credentials) => axios.post('/users/signup', credentials),
      currentUser: () => axios.get(`/users/self`),
      addToFavorites: (id) => axios.patch(`/users/favorites/add/${id}`),
      removeFromFavorites: (id) => axios.patch(`/users/favorites/remove/${id}`),
    };
  },
  statistics() {
    return {
      ratingByYear: () => axios.get('/statistics/yearsRating'),
      favoriteMovies: () => axios.get('/statistics/favoriteMovies'),
      genreDistribution: () => axios.get('/statistics/genreDistribution'),
    };
  },
};

updateAuthorizationHeader();
