const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: { type: String },
  password: { type: String },
  isAdmin: { type: Boolean},
  favorites: { type: Array },
});

const User = mongoose.model("users", userSchema);

module.exports = User;
