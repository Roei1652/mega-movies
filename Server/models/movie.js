const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema({
  title: { type: String },
  description: { type: String },
  duration: { type: Number },
  date: { type: Date },
  rating: { type: Number },
  path: { type: String },
  genre: {type: String }
});

const Movie = mongoose.model("movies", movieSchema);

module.exports = Movie;
