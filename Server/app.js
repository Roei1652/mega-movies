const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const db = require('./db/db');
const authenticateToken = require("./middlewares/authenticateToken");
const cors = require("cors");
const fileUpload = require("express-fileupload");

const port = 3000;

const moviesRouter = require('./routes/movies');
const usersRouter = require('./routes/users');
const statisticsRouter = require('./routes/statistics');

const app = express();

// view engine setup
// eslint-disable-next-line no-undef
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// eslint-disable-next-line no-undef
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors())
app.use(fileUpload())

// add auth middleware
app.use('/users', usersRouter);
app.use('/movies', authenticateToken, moviesRouter);
app.use('/statistics', statisticsRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

db.connect(() => {
  app.listen(port, () => {
    try {
      require('./db/generator').generate(1000);
    } catch {
      console.log('well....')
    }

    console.log(`app is listening on port ${port}`);
  });
});

module.exports = app;
