const Movie = require("../models/movie");
const axios = require("axios");
const fs = require('fs');

const generate = async (amount) => {
    const IMAGES_API_URL = 'https://picsum.photos/200/300';
    const WORD_API_URL = 'https://san-random-words.vercel.app';

    const genres = ['sci-fi', 'comedy', 'action'];

    for (let movieIndex = 0; movieIndex < amount; movieIndex++) {
        const path = `./data_center/General Movies/${movieIndex}`;
        fs.mkdirSync(path)

        fs.copyFile('./data_center/General Movies/video.mp4', `${path}/video.mp4`, (err) => {
            if (err) {
                console.log(err)
            };
        });

        let imageData = (await axios.get(IMAGES_API_URL, { responseType: "arraybuffer" })).data;
        fs.writeFile(`${path}/poster.png`,  Buffer.from(imageData, "binary").toString("base64"), 'base64', (err) => {
            if (err) {
                console.log(err);
            }
        });

        let currMovie = new Movie();
        let randomWord = (await axios.get(WORD_API_URL)).data[0].word;

        currMovie.title = randomWord;
        currMovie.description = "description" + randomWord;
        currMovie.date = randomDate(new Date(2008, 0, 1), new Date())
        currMovie.rating = Math.random() * (5.0 - 1.0) + 1.0;
        currMovie.path = path;
        currMovie.genre = genres[Math.floor(Math.random() * genres.length)];;
        currMovie.save();
    }
}

randomDate = (start, end) => {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

module.exports = {
    generate
}