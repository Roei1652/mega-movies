const mongoose = require("mongoose");
const mongodbUrl = "mongodb+srv://roei:Roei1652*@cluster0.hucws.mongodb.net";
const dbName = "MegaMovies";

const connect = (callback) => {
  mongoose.connect(`${mongodbUrl}/${dbName}`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  callback();
};

module.exports = {
  connect,
};
