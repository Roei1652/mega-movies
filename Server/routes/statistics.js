const express = require('express');
const router = express.Router();

const Movie = require("../models/movie");
const User = require('../models/user')

const MOVIES_PER_PAGE = 20;

router.get('/yearsRating', async (req, res) => {
    const ratings = await Movie.aggregate([{$group: {_id: {$year: "$date"}, rating: {$avg: "$rating"}}},
{$sort: { '_id': 1}}]).exec();
    res.send(ratings.map(rating => {
        return {year: rating._id, rating: rating.rating}
    }))
})

router.get('/favoriteMovies', async (req, res) => {
    const usersFavorites = await User.aggregate([
        { $unwind: "$favorites" },
        { $group: {_id: "$favorites", "movieCount": {$sum: 1}} },
        { $sort: {'movieCount': -1} },
        { $limit : 5 },
        { "$lookup" : { from: "movies", localField: "_id", foreignField: "_id", as: "movie" } },
        { $project: {title: { $arrayElemAt: ["$movie.title", 0] }, count: "$movieCount"} }
    ]).exec();
    res.send(usersFavorites);
})

router.get('/genreDistribution', async (req, res) => {
    const genreDistribution = await Movie.aggregate([
        { $group: {_id: "$genre", "count": {$sum: 1}} },
    ]).exec();
    res.send(genreDistribution.map(gd => {
        return {genre: gd._id, count: gd.count }
    }));
})

module.exports = router;