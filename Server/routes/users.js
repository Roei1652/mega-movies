require('dotenv').config();

const express = require('express');

const router = express.Router();

const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = require('../models/user');
const cryptoJs = require('crypto-js');

const authenticateToken = require('../middlewares/authenticateToken');

router.post('/login', async (req, res) => {
  const user = await User.findOne(
    {
      username: req.body.username,
      password: cryptoJs.MD5(req.body.password).toString(),
    },
    {
      _id: false,
      username: true,
      isAdmin: true,
    },
  ).exec();

  if (user) {
    const accessToken = jwt.sign(
      {
        username: user.username,
        isAdmin: user.isAdmin,
      },
      process.env.ACCESS_TOKEN_SECRET,
    );

    res.send({
      user,
      token: accessToken,
    });
  } else {
    res.sendStatus(401);
  }
});

router.post('/signup', async (req, res) => {
  const userExists = await User.findOne({
    username: req.body.username,
    password: cryptoJs.MD5(req.body.password).toString(),
  }).exec();

  if (userExists) {
    res.sendStatus(409);
  } else {
    const user = new User();
    user.username = req.body.username;
    user.password = cryptoJs.MD5(req.body.password).toString();
    user.isAdmin = false;
    user.favorites = [];

    user.save();
    res.sendStatus(200);
  }
});

router.get('/self', authenticateToken, (req, res) => {
  res.send({
    username: req.user.username,
    isAdmin: req.user.isAdmin,
  });
});

router.patch('/favorites/add/:_id', authenticateToken, async (req, res) => {
  await User.updateOne(
    { username: req.user.username },
    { $push: { favorites: new mongoose.Types.ObjectId(req.params._id) } },
  ).exec();

  res.sendStatus(200);
});

router.patch('/favorites/remove/:_id', authenticateToken, async (req, res) => {
  await User.updateOne(
    { username: req.user.username },
    { $pull: { favorites: new mongoose.Types.ObjectId(req.params._id) } },
  ).exec();

  res.sendStatus(200);
});

module.exports = router;
