const express = require('express');
const router = express.Router();
const fs = require('fs');
const rimraf = require('rimraf')

const Movie = require("../models/movie");
const User = require("../models/user");

const MOVIES_PER_PAGE = 20;

router.get('/pages/:pageNumber', async (req, res) => {
  res.send(await Movie.find({})
  .skip(req.params.pageNumber > 0 ? ( ( req.params.pageNumber - 1 ) * MOVIES_PER_PAGE ) : 0)
  .limit(MOVIES_PER_PAGE).exec());
});

router.get('/movie/:_id', async (req, res) => {
  const path = `${(await Movie.findById(req.params._id)).path}/video.mp4`;
  const contents = fs.readFileSync(path, {encoding: 'base64'});
  res.send({video: contents})
});

router.get('/poster/:_id', async (req, res) => {
  const path = `${(await Movie.findById(req.params._id)).path}/poster.png`;
  const contents = fs.readFileSync(path, {encoding: 'base64'});
  res.send({poster: contents})
});

router.get('/favorites', async (req, res) => {
  const user = await User.findOne({username: req.user.username}).exec();
  res.send(await Movie.find({_id: {$in: user.favorites}}).exec());
});

router.post('/upload', async (req, res) => {
  const path = `./data_center/${req.body.title}`;
  fs.mkdirSync(path)
  fs.appendFile(`${path}/video.mp4`, req.files.video.data, (err) => console.log(err));
  fs.appendFile(`${path}/poster.png`, req.files.poster.data, (err) => console.log(err));
  
  const movie = new Movie();
  movie.title = req.body.title;
  movie.description = req.body.description;
  movie.date = new Date(req.body.date)
  movie.rating = 2.5;
  movie.path = path;
  movie.genre = req.body.genre;
  movie.save();

  res.sendStatus(200);
})

router.delete('/delete/:_id', async (req, res) => {
  const movie = await Movie.findByIdAndDelete(req.params._id).exec();
  rimraf.sync(movie.path);
  
  res.sendStatus(200);
})

module.exports = router;
